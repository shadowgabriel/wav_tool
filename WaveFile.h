#pragma once

class WaveFile {
	FILE* audio_file;
	char * buffer;
	bool is_set = false;
	int data_offset;
	int file_size;
	int nr_of_bytes;
	char file_addres[_MAX_PATH];
	char chunk_ID[5];
	int chunk_size;
	char format[5];
	char subchunk1_ID[5];
	int subchunk1_size;
	int audio_format;
	int num_channels;
	int sample_rate;
	int byte_rate;
	int block_align;
	int bits_per_sample;
	char subchunk2_ID[5];
	int subchunk2_size;

	int read_bytes(char*, int, int);

public:
	WaveFile(const char*);
	int wave_file_open();
	void show_info();
	int wave_file_close();
};
