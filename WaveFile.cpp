#include <iostream>
#include <fstream>
using namespace std;

#include "WaveFile.h"

WaveFile::WaveFile(const char* file_addres) {
	strcpy(WaveFile::file_addres, file_addres);
}

int WaveFile::read_bytes(char* input_vector, int index, int how_many_bytes) {
	int temp = 0;
	for (int i = 0; i < how_many_bytes; i++) {
		temp += ((uint8_t)input_vector[index + i]) << (i * 8);
	}
	return temp;
}

int WaveFile::wave_file_open() {
	audio_file = fopen(file_addres, "rb");
	if (audio_file == NULL) {
		/* file error */
		return 1;
	}
	fseek(audio_file, 0, SEEK_END);
	file_size = ftell(audio_file);
	rewind(audio_file);
	buffer = (char*)malloc(sizeof(char)*file_size);
	if (buffer == NULL) {
		/* memory error */
		return 2;
	}
	nr_of_bytes = fread(buffer, 1, file_size, audio_file);
	if (nr_of_bytes != file_size) {
		/* reading error */
		return 3;
	}

	memcpy(chunk_ID, buffer, 4);
	chunk_ID[4] = '\0';

	chunk_size = read_bytes(buffer, 4, 4);

	memcpy(format, buffer + 8, 4);
	format[4] = '\0';

	memcpy(subchunk1_ID, buffer + 12, 4);
	subchunk1_ID[4] = '\0';

	subchunk1_size = read_bytes(buffer, 16, 4);

	audio_format = read_bytes(buffer, 20, 2);
	num_channels = read_bytes(buffer, 22, 2);
	sample_rate = read_bytes(buffer, 24, 4);
	byte_rate = read_bytes(buffer, 28, 4);
	block_align = read_bytes(buffer, 32, 2);
	bits_per_sample = read_bytes(buffer, 34, 2);

	memcpy(subchunk2_ID, buffer + 36, 4);
	subchunk2_ID[4] = '\0';
	int i = 0;
	while (strcmp(subchunk2_ID, "data")) {
		i++;
		memcpy(subchunk2_ID, buffer + 36 + i, 4);
		subchunk2_ID[4] = '\0';
	}

	subchunk2_size = read_bytes(buffer, 36 + i + 4, 4);
	data_offset = 36 + i + 8;

	is_set = true;
	/* no error */
	return 0;
}

void WaveFile::show_info() {
	if (is_set) {
		cout << "chunk ID = " << chunk_ID << '\n';
		cout << "chunk size = " << chunk_size << " bits\n";
		cout << "format = " << format << '\n';
		cout << "subchunk ID_1 = " << subchunk1_ID << '\n';
		cout << "subchunk_1 size = " << subchunk1_size << '\n';
		if (audio_format == 1) {
			cout << "audio format = linear quantization\n";
		} else {
			cout << "audio format = compression (" << audio_format << ")\n";
		}
		if (num_channels == 1) {
			cout << "number of channels = mono\n";
		} else if (num_channels == 2) {
			cout << "number of channels = stereo\n";
		} else {
			cout << "number of channels = undifined (" << num_channels << ")\n";
		}
		cout << "sample rate = " << sample_rate << " Hz\n";
		cout << "byte rate = " << byte_rate << " bits / s\n";
		cout << "block alignment = " << block_align << " byts / sample\n";
		cout << "bits per sample = " << bits_per_sample << " bits / sample\n"; 
		cout << "subchunk ID_2 = " << subchunk2_ID << '\n';
		cout << "subchunk_2 size = " << subchunk2_size << " bits\n";
	} else {
		cout << "You must first read the audio file\n";
	}
}


int WaveFile::wave_file_close() {
	fclose(audio_file);
	free(buffer);
	return 0;
}


